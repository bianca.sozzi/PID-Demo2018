package com.mercedes.pid_demo2018

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.content.ContextCompat
import com.Company.demo.UnityPlayerActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.mercedes.pid_demo2018.TextRecognition.TextCaptureActivity

/**
 * Created by Mercedes on 17/4/2018.
 */
class MapsService: Service() {
    override fun onBind(p0: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private var lastMarker: Marker? = null
    private var previousMarker: Marker? = null
    private var level: Int? = 0
    private var isNotify: Boolean = false

    //notificacion
    private lateinit var notificationManager: NotificationManager
    private val ID_NOTIFICATION = 123

    //preferencias
    private lateinit var preferences: SharedPreferences
    private var PREFERENCE_NAME = "shared preferences"

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTING = 2
    }

    override fun onCreate() {
        super.onCreate()
        preferences = getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = preferences.edit()
        editor.putInt("level", 0)
        editor.apply()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation

                //comparacion ubicaciones seleccionadas
                if (lastLocation.latitude <= -32.8970 && lastLocation.latitude >= -32.8972 && lastLocation.longitude <= -68.8532 && lastLocation.longitude >= -68.8535) {
                    var vibrate = longArrayOf(0, 100, 100)

                    if (!isNotify) {
                        level = preferences.getInt("level", 0)

                        when (level) {
                            0 -> {
                                val cam: Intent = Intent(applicationContext, UnityPlayerActivity::class.java)
                                val pcam: PendingIntent = PendingIntent.getActivities(applicationContext, 0, arrayOf(cam), 0)
                                var not: Notification = Notification.Builder(applicationContext).setSmallIcon(android.R.drawable.ic_menu_camera).setContentTitle("Conocimiento por descubrir!").setContentText("Presiona para descubrir! DESDE SERVICE level: " + level).setVibrate(vibrate).setContentIntent(pcam).build()
                                notificationManager.notify(ID_NOTIFICATION, not)
                            }
                            1 -> {
                                val cam: Intent = Intent(applicationContext, UnityPlayerActivity::class.java)
                                val pcam: PendingIntent = PendingIntent.getActivities(applicationContext, 0, arrayOf(cam), 0)
                                var not: Notification = Notification.Builder(applicationContext).setSmallIcon(android.R.drawable.ic_menu_camera).setContentTitle("Conocimiento por descubrir!").setContentText("Presiona para descubrir! level: " + level).setVibrate(vibrate).setContentIntent(pcam).build()
                                notificationManager.notify(ID_NOTIFICATION, not)
                            }
                            2 -> {
                                val cam: Intent = Intent(applicationContext, TextCaptureActivity::class.java)
                                val pcam: PendingIntent = PendingIntent.getActivities(applicationContext, 0, arrayOf(cam), 0)
                                var not: Notification = Notification.Builder(applicationContext).setSmallIcon(android.R.drawable.ic_menu_camera).setContentTitle("Conocimiento por descubrir!").setContentText("Presiona para descubrir! level: " + level).setVibrate(vibrate).setContentIntent(pcam).build()
                                notificationManager.notify(ID_NOTIFICATION, not)


                            }
                            else -> {
                                val cam: Intent = Intent(applicationContext, MainActivity::class.java)
                                val pcam: PendingIntent = PendingIntent.getActivities(applicationContext, 0, arrayOf(cam), 0)
                                var not: Notification = Notification.Builder(applicationContext).setSmallIcon(android.R.drawable.ic_menu_camera).setContentTitle("Ya descubriste todo!").setContentText("Presiona ver la app, level: " + level).setVibrate(vibrate).setContentIntent(pcam).build()
                                notificationManager.notify(ID_NOTIFICATION, not)
                            }
                        }
                    }

                } else {
                    //notificationManager.cancel(ID_NOTIFICATION)
                }
            }

        }
        createLocationRequest()
    }

    private fun startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this as Activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 1000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                  //  exception.startResolutionForResult(this as Activity, REQUEST_CHECK_SETTING)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}