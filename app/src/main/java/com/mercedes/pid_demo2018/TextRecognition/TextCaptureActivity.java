package com.mercedes.pid_demo2018.TextRecognition;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.mercedes.pid_demo2018.R;

import java.io.IOException;

public class TextCaptureActivity extends AppCompatActivity {

    private SurfaceView cameraView;
    private Button goToLink;
    private CameraSource cameraSource;
    private static final String TAG = "TextCaptureActivity";
    private final int MY_PERMISSION_REQUEST_CAMERA = 1;

    private SharedPreferences preferences;
    private String PREFERENCE_NAME = "shared preferences";
    private int level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_reco);

        cameraView = findViewById(R.id.surface_view);
        goToLink = findViewById(R.id.button_goToLink);
        goToLink.setVisibility(View.GONE);
        createCameraSource();

        goToLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri url = Uri.parse("http://www.ehu.eus/juancarlos.gorostizaga/mateI15/T_matrdeter/MatrDeter");
                Intent intent = new Intent(Intent.ACTION_VIEW, url);
                startActivity(intent);
            }
        });


    }

    private void enableButton(){
        goToLink.setVisibility(View.VISIBLE);
        goToLink.setText("Ver el nuevo tema");
    }

    @SuppressLint("InlinedApi")
    private void createCameraSource(){
        Context context = getApplicationContext();

        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();

        if(!textRecognizer.isOperational()){
            Log.w(TAG, "Detector dependencies are not yet available");
            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowStorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowStorageFilter) != null;

            if(hasLowStorage){
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_SHORT).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<TextBlock> detections) {
                final SparseArray<TextBlock> items = detections.getDetectedItems();
                if(items.size() != 0){
                    StringBuilder value = new StringBuilder();
                    for(int i = 0; i<items.size();i++){
                        TextBlock item = items.valueAt(i);
                        value.append(item.getValue());
                        value.append("\n");

                        Log.i("Texto Detectado: ", value.toString());

                        if(item.getValue().contains("Determinante")|| item.getValue().contains("determinante")||item.getValue().contains("DETERMINANTE")){
                                runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    enableButton();
                                }
                            });
                            break;
                        }
                    }
                }
            }

        });

        cameraSource = new CameraSource.Builder(context,textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setRequestedFps(15.0f)
                .setAutoFocusEnabled(true)
                .build();
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                //Verificar los permisos de la camara
                if(ActivityCompat.checkSelfPermission(TextCaptureActivity.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        if(shouldShowRequestPermissionRationale(Manifest.permission.CAMERA));
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSION_REQUEST_CAMERA);
                    }
                    return;
                }else{
                    try{
                        cameraSource.start(cameraView.getHolder());
                    }catch (IOException ie){
                        Log.e("CAMERA SOURCE", ie.getMessage());
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
    }

    @Override
    protected void onResume() {
        preferences = getSharedPreferences(PREFERENCE_NAME,MODE_PRIVATE);
        level = preferences.getInt("level",0);
        level += 1;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("level",level);
        editor.apply();

        super.onResume();
        createCameraSource();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraSource.release();
    }

}
