package com.mercedes.pid_demo2018

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.Company.demo.UnityPlayerActivity
import com.mercedes.pid_demo2018.TextRecognition.TextCaptureActivity
import kotlinx.android.synthetic.main.activity_main.*
import com.mercedes.pid_demo2018.MapsActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(!runtime_permission()){
            enableButtons()
            startService()
        }

    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            Button_mapa.id -> {
                val mapa = Intent(this, MapsActivity::class.java)
                startActivity(mapa)
            }

            Button_camara.id -> {
                val unity = Intent(this, UnityPlayerActivity::class.java)
                startActivity(unity)
            }

            Button_textReco.id -> {
                val textRecognition = Intent(this, TextCaptureActivity::class.java)
                startActivity(textRecognition)
            }


        }
    }


    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Estas seguro?")
        builder.setMessage("Desea dejar de recibir notificaciones de la app?")
        builder.setPositiveButton("Si",{ dialogInterface: DialogInterface, i: Int ->
            var stop= Intent(applicationContext,MapsService::class.java)
            stopService(stop)
            finish()
            Toast.makeText(this,"Servicio detenido",Toast.LENGTH_SHORT).show()
        })
        builder.setNegativeButton("No", { dialogInterface: DialogInterface, i: Int ->
            super.onBackPressed()
        })
        builder.show()
    }

    private fun runtime_permission(): Boolean{
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION) ,100)
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 100){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){

            }else{
                runtime_permission()
            }
        }
    }
    private fun enableButtons(){
        Button_mapa.setOnClickListener(this)
        Button_camara.setOnClickListener(this)
        Button_textReco.setOnClickListener(this)

    }

    private fun startService(){
        val service = Intent(this, MapsService::class.java)
        startService(service)
        Toast.makeText(this,"Servicio iniciado",Toast.LENGTH_SHORT).show()
    }
}
