package com.mercedes.pid_demo2018

import android.app.Activity
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.graphics.drawable.Icon
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.Company.demo.UnityPlayerActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mercedes.pid_demo2018.TextRecognition.TextCaptureActivity

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private var lastMarker: Marker? = null
    private var previousMarker: Marker? = null
    private var level: Int? = 0
    private var isNotify: Boolean = false

    //notificacion
    private lateinit var notificationManager: NotificationManager
    private val ID_NOTIFICATION =123

    //preferencias
    private lateinit var preferences: SharedPreferences
    private var PREFERENCE_NAME = "shared preferences"

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE= 1
        private const val REQUEST_CHECK_SETTING = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        preferences = getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        var editor: SharedPreferences.Editor = preferences.edit()
        editor.putInt("level",0)
        editor.apply()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation

                placeMarkerOnMap(LatLng(lastLocation.latitude, lastLocation.longitude),lastLocation)
            }

        }
        createLocationRequest()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMapClickListener { this }

        setUpMap()
    }

    private fun setUpMap(){
        if( ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true //habilito boton de ubicación propia

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if(location != null){
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng,lastLocation)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 18.0f))

            }
        }
    }

    override fun onMarkerClick(p0: Marker?)=false

    private fun placeMarkerOnMap(location: LatLng, location1: Location ){
        val markerOptions = MarkerOptions().position(location)
        // PARA CAMBIAR ICONO DEL MARKER
        //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(resources, R.mipmap.__ic_user_location_png)))
        markerOptions.title("lat:"+location1.latitude+" y long:"+location1.longitude)
        if(lastMarker != null){
            lastMarker?.remove()
        }
        lastMarker = map.addMarker(markerOptions)
    }

    private fun startLocationUpdates(){
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        fusedLocationClient.requestLocationUpdates(locationRequest,locationCallback, null)
    }

    private fun createLocationRequest(){
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { exception ->
            if(exception is ResolvableApiException){
                try{
                    exception.startResolutionForResult(this@MapsActivity, REQUEST_CHECK_SETTING)
                }catch (sendEx: IntentSender.SendIntentException){

                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTING) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }


    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    override fun onBackPressed() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
        super.onBackPressed()
    }
}



